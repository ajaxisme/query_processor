#!/bin/bash

make
echo "Generating intermediate files...."
./indexer
echo "Merging sorted intermediate files into 1 file...."
sort -m -k1,1 -k2,2n -k3,3n *_temp > temp_inverted_index
echo "Generating final inverted list"
./make_index
./query_processor
