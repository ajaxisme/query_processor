#include<algorithm>
#include<iostream>
#include<cmath>
#include<cstdio>
#include<cstdlib>
#include<fstream>
#include<map>
#include<sstream>
#include<vector>

using namespace std;

#define MAX_RESULTS 10

struct word_index{
    string term;
    long long startpos;
    long long endpos;
    long long size;
};

struct doc_info{
    long long docID;
    string url;
    string ip;
    int docSize;
};

struct common_doc{
    string url;
    double BM25score;
};

double avg_docSize;
long long total_docs;

// Function to decompress a single block sompressed using varbyte
// and return list of all the integers
vector<long long> decompress_block(string compressed_str){

    unsigned char ch;

    vector<long long> numbers;
    int byte;
    ch = compressed_str[0];
    long long n=ch;
    
    for(int i=1;i<compressed_str.size();i++){

        ch = compressed_str[i];
        if(ch >= 128)
            byte = ch-128;
        else
            byte = ch;

        n = 128*n + byte;
        
        if(ch < 128){
            numbers.push_back(n/128);
            n = ch;
        }
    }

    numbers.push_back(n);

    return numbers;
}

// Function to look at only the first value of the 
// compressed block
long long peek_first_in_block(string compressed_str){

    unsigned char ch;

    vector<long long> numbers;
    int byte;
    ch = compressed_str[0];
    long long n=ch;
    
    for(int i=1;i<compressed_str.size();i++){

        ch = compressed_str[i];
        if(ch >= 128)
            byte = ch-128;
        else
            byte = ch;

        n = 128*n + byte;
        
        if(ch < 128){
            n = n/128;
            break;
        }
    }

    return n;
}

// Open inverted list for a query term, corresponds to the openList function mentioned
// in class
vector<string> open_inverted_list(ifstream& in, map<string,word_index>& terms, string term){

    // Start querying
    long long startpos = terms[term].startpos-1;
    long long endpos = terms[term].endpos;

    in.seekg(startpos);

    long long length = endpos - startpos;
    unsigned char* buffer = (unsigned char*) malloc (length*(sizeof(unsigned char*)));

    in.read((char*) (&buffer[0]), length);

    vector<string> blocks;
    string block;

    for(int i=0;i<length;i++){
        if(buffer[i] == char(0)){
            blocks.push_back(block);
            block.clear();
            continue;
        }
        block.push_back(buffer[i]);
    }

    blocks.push_back(block);

    return blocks;
}

// Get corresponding frequency of the docID in the block.
// Corresponds to the getFreq function mentioned in class
long long getFreq(string frequency_block, int pos){

    return decompress_block(frequency_block)[pos];

}

// Get the next block id with docID greater than or equal to the 
// requested docID
int nextGEQ(vector<string>& compressed_inverted_list, long long docID){

    int i;
    for(i=0;i<compressed_inverted_list.size();i++){
        long long peeked = peek_first_in_block(compressed_inverted_list[i]);
        if(peeked == docID)
            return i;
        if(peeked > docID)
            break;
    }

    return i-1;
}

// Divide a combined query into separate terms and returns them as a list
vector<string> get_query(string query, map<string,word_index>& terms){

    vector<string> query_terms;
    string query_term;
    stringstream ss(query);

    while(getline(ss, query_term,' ')){
        if(terms.find(query_term) != terms.end())
            query_terms.push_back(query_term);
    }

    return query_terms;
}

// Restore the docIDs for the block compressed using the immediate difference
// of the previous docID in the block
vector<long long> restore_original_docs(vector<long long>& docs){

    long long current_docID = docs[0];
    for(int i=1;i<docs.size();i++){
        docs[i] = current_docID + docs[i];
        current_docID = docs[i];
    }

    return docs;
}

// Find the docID in the given list of docs in a block
int find_doc(vector<long long>& docs, long long docID){

    int start = 0, end = docs.size()-1;
    int mid;
    while(start<=end){
        mid = (start + end)/2;
        if(docs[mid] == docID)
            return mid;
        else if(docs[mid] > docID)
            end = mid - 1;
        else
            start = mid + 1;
    }
    

   /*
   for(int i=0;i<docs.size();i++){
       if(docs[i] == docID){
           return i;
       }
   } 
   */

   return -1;
}

// Compute the BM25 score for all the terms in the current doc
double getBM25score(vector<long long>& query_freq, int docSize, vector<long long>& frequency){

    double k1 = 1.2;
    double b = 0.75;
    double K = k1*((1-b) + (b*docSize/avg_docSize));

    double sum = 0.0;

    for(int i=0;i<query_freq.size();i++){
        double first = (total_docs - query_freq[i] + 0.5)/(query_freq[i] + 0.5);
        first = log(first);
        double second = ((k1+1)*frequency[i])/(K+frequency[i]);

        sum = sum + (first*second);
    }

    return sum;
    
}

// Sort by decreasing BM25 score
bool compareScore(common_doc& a, common_doc& b){

    if(a.BM25score > b.BM25score)
        return true;
    else
        return false;
}

// Main function to perform search a query string given via the command line
void perform_search(ifstream& in, vector<doc_info>& doc_index, map<string,word_index>& terms, string query){
    
    vector<string> query_terms = get_query(query, terms);

    // Check to see if none of the words are present in the documents
    if(query_terms.size() == 0){
        cout<<"Query terms are not in the index"<<endl;
        return;
    }

    vector<long long> common_docIDs;
    vector<vector<long long> > common_frequencies;

    vector<vector<string> > inverted_lists(query_terms.size());

    // Find shortest list among the inverted lists
    int shortest_list = 0;
    long long min = terms[query_terms[0]].size;

    for(int i=0;i<query_terms.size();i++){
        inverted_lists[i] = open_inverted_list(in, terms, query_terms[i]);
        if(terms[query_terms[i]].size <= min){
            min = terms[query_terms[i]].size;
            shortest_list = i;
        }
    }
    
    int i=0;

    // Iterate over the docIDs in the shortest list to find common docs among all the query terms
    while(i<inverted_lists[shortest_list].size()){

        // Decompress the first block of the shortest inverted list
        vector<long long> docs = decompress_block(inverted_lists[shortest_list][i]);

        // Restore docIDs for the block
        docs = restore_original_docs(docs);

        vector<long long> freq = decompress_block(inverted_lists[shortest_list][i+1]);

        vector<long long> frequencies(inverted_lists.size(), 0);

        int k=0;

        while(k<docs.size()) {

            // For every docID in the first list, find the common docID among all other query terms
            long long current_docID = docs[k];

            frequencies[shortest_list] = freq[k];

            bool found=true;

            for(int j=0;j<query_terms.size();j++){

                if(j == shortest_list)
                    continue;

                int blockID = nextGEQ(inverted_lists[j], current_docID) - 1;
                if(blockID == -1){
                    found = false;
                    break;
                }

                vector<long long> temp_docs = decompress_block(inverted_lists[j][blockID]);
                temp_docs = restore_original_docs(temp_docs);

                // last docID in block is less than current doc, move to next current_doc
                if(temp_docs[temp_docs.size()-1] < current_docID){
                    found = false;
                    break;
                }

                int position = find_doc(temp_docs, current_docID);

                if(position == -1){
                    found = false;
                    break;
                }

                //Get Corresponding Frequency
                frequencies[j] = getFreq(inverted_lists[j][blockID+1], position);
                //frequencies[j] = decompress_block(inverted_lists[j][blockID+1])[position];
            }

            if(!found){
                k++;
                continue;
            }

            // Common docID is found
            common_docIDs.push_back(current_docID);
            common_frequencies.push_back(frequencies);

            k++;
        }
        i = i + 2;
    }

    // Check to see if there were no matching documents
    if(common_docIDs.size() == 0){
        cout<<"No matching document"<<endl;
        return;
    }

    vector<common_doc> common_docs;
    for(int i=0;i<common_docIDs.size();i++){
        common_doc d;
        d.url = doc_index[common_docIDs[i]].url;
        vector<long long> query_freq;
        for(int j=0;j<query_terms.size();j++){
            query_freq.push_back(terms[query_terms[j]].size);
        }
        d.BM25score = getBM25score(query_freq, doc_index[common_docIDs[i]].docSize, common_frequencies[i]);
        common_docs.push_back(d);
    }

    // Only print the top results according to BM25 score
    sort(common_docs.begin(), common_docs.end(), compareScore);
    int max = MAX_RESULTS<common_docs.size()?MAX_RESULTS:common_docs.size();
    for(int i=0;i<max;i++)
        cout<<common_docs[i].url<<" "<<common_docs[i].BM25score<<endl;

}

int main(){

    string query;

    ifstream in("compressed_inverted_index", ios::binary | ios::in);
    ifstream wi("word_index", ios::binary | ios::in);
    ifstream doclist("document_list", ios::binary | ios::in);

    // Get word index in memory
    map<string, word_index> terms;
    string line;
    while(getline(wi, line)){
        stringstream ss;
        ss.str(line);
        word_index w;
        ss >> w.term >> w.startpos >> w.endpos >> w.size;
        terms[w.term] = w;
    }

    long long sum = 0;
    long long docs = 0;

    // Get document index in memory
    vector<doc_info> doc_index;

    while(getline(doclist, line)){
        stringstream ss(line);
        doc_info d;
        ss >> d.docID >> d.url >> d.ip >> d.docSize;
        sum = sum + d.docSize;
        docs++;
        doc_index.push_back(d);
    }

    avg_docSize = (double)sum/docs;
    total_docs = docs;

    // Keep performing the query till user enters q
    cout<<"Enter the query(q to exit): ";
    while(getline(cin, query)){
        if(query == "q")
            break;
        perform_search(in, doc_index, terms, query);
        cout<<"\nEnter the query(q to exit): ";
    }

    in.close();
    wi.close();
    doclist.close();

    cout<<endl;
    return 0;
}

