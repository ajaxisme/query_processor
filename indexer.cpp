#include <algorithm>
#include <cctype>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <fstream>
#include <iostream>
#include <sstream>
#include <vector>
#include <map>

#include "zlib.h"

#define MAX_MEMORY 536870912 // 200MB
#define DEBUG 1

#define FIRST_HEADER 276
#define CHUNK_SIZE 450

using namespace std;

/* Posting strcuture */
struct posting{
    long long docID;
    string term;
    int frequency;
};

/* Lexicon structure */
struct lexicon_list {
    string term;
    int termID; //optional
    int file_pos; //pointer to position in inverted list
};


/* Function to open a gzipped file and Return the file pointer */
gzFile openGZFile(string filename) {
    // Open a gzipped file and return file pointer
    //
    gzFile gzPtr = gzopen(filename.c_str(), "rb");
    if(gzPtr == NULL){
        cout<<"Error opening file: "<<filename<<endl;
        return NULL;
    }

    return gzPtr;
}

/* Function to read an entire zipped file and return a string containing contents */
string readEntireGzippedFile(string filename) {
    // Return entire gzipped index file as string(unzipped)

    gzFile gzPtr = openGZFile(filename);

    unsigned char buffer[8192];
    unsigned int unzipped_bytes;
    vector<unsigned char> unzippedData;

    while(true) {
        unzipped_bytes = gzread(gzPtr, buffer, 8192);
        if (unzipped_bytes > 0) {
                for(int i=0;i<unzipped_bytes;i++)
                    unzippedData.push_back(buffer[i]);
        }
        else
            break;
    }

    string content(unzippedData.begin(), unzippedData.end());

    gzclose(gzPtr);

    return content;
}

/* Function to read a chunk of opened gzipped file and return contents as string */
string readChunk(gzFile gzPtr, string filename, int len) {
    // Return one document as string (unzipped format)

    if (gzPtr == NULL) {
        gzPtr = openGZFile(filename);
        // Check for error in opening file
    }

    unsigned char* buffer = (unsigned char*) malloc (len*(sizeof(unsigned char*)));
    unsigned int unzipped_bytes;
    vector<unsigned char> unzippedData;

    unzipped_bytes = gzread(gzPtr, buffer, len);
    for(int i=0;i<unzipped_bytes;i++){
        unzippedData.push_back(buffer[i]);
    }

    string chunk(unzippedData.begin(), unzippedData.end());

    free(buffer);

    return chunk;
}

/* Sort function for  postings first based on termID and then docID */
bool compare_postings(posting a, posting b) {
    // First compare based on term
    // If equal, compare based on docID
    //
    if(a.term == b.term) {
        if(a.docID == b.docID) {
            return a.frequency < b.frequency;
        }
        else
            return a.docID < b.docID;
    }
    else
        return a.term < b.term;
}

/* Write intermediate postings list to disk */
void write_postings_to_disk(int tempID, vector<posting> postings) {
    string filename = to_string(tempID).append("_temp");
    ofstream temp_file (filename, ios::out | ios::binary);
    for(int i=0;i<postings.size();i++) {
        string str = postings[i].term + " " + to_string(postings[i].docID) + " " + to_string(postings[i].frequency) + "\n";
        temp_file << str;
    }

    temp_file.close();
}

int parse_first_header(string header){
    stringstream ss;
    string line;
    ss.str(header);
    string field;
    string value;
    char ch;
    while(getline(ss, line)){
        stringstream ss1;
        ss1.clear();
        ss1.str(line);
        ss1 >> field >> value;
        if(field == "Content-Length:"){
            return stoi(value);
        }
    }

    return -1;
}

pair<string, int> parse_header(string header){
    cout<<header<<endl;
    stringstream ss;
    string line;
    string url;
    ss.str(header);
    string field;
    string value;
    while(getline(ss, line)){
        stringstream ss1;
        ss1.clear();
        ss1.str(line);
        ss1 >> field >> value;
        if(field == "WARC-Target-URI:"){
            url = value;
        }
        if(field == "Content-Length:"){
            return make_pair(url, stoi(value));
        }
    }

    return make_pair("",-1);
}

int find_header_end(string data){

    string header;
    int header_len = data.size();

    stringstream ss(data);
    while(getline(ss, header, '\n')){
        header_len = header_len - header.size() - 1;
        if(header=="\r")
            return header_len;
    }

    /*

    for(int j=data.size()-1;j>=0;j--){
        if(data[j] == '\r' && (data[j-1] == '\n' || data[j-1] == '\r'))
            return (data.size()-j-2);
    }
    */

    return -1;
}


/* Main driver file to perform indexing */
int main() {

    /* For every index files, do the following
     * 1. Get the contents of the gzipped index file into memory
     * 2. Uncompress the contents in memory
     * 3. Open corresponding data file in gzipped format
     * 4. For every line in index file (corresponding to docID):
     *      1. Increment docID and assign that to URL
     *      2. Read contents till the content length mentioned
     *      3. Parsee the contents to extract all keywords for that particular doc
     *      4. Initialize DocLength to 0
     *      5. For every keyword:
     *          1. Get frequency and position
     *          2. Generate a posting as (docID, term, frequency, pos1, pos2)
     *          3. Write to a new file
     *          4. If keyword present, increase its frequency in term table
     *             else, add to the term table with frequency 1
     *          5. Increase DocLength by 1
     *      6. Write the (docID, url, IPAddress, DocLength) to another data strcuture
    */

    /* For all the listings, merge sort by (term, docID, frequency) */

    ofstream doc_ptr ("document_list", ios::out | ios::binary);

    int tempID = 0;
    long long docID = 0;
    int zipped_file_index=0;
    vector<posting> postings;

    while(zipped_file_index < 1) {

        string data_file = "CC-MAIN-20160428161506-00003-ip-10-239-7-51.ec2.internal.warc.wet.gz";
        gzFile gzDataPtr = openGZFile(data_file);

        string first_header_chunk = readChunk(gzDataPtr, data_file, FIRST_HEADER);
        int move_next = parse_first_header(first_header_chunk);
        gzseek(gzDataPtr, move_next + 4, SEEK_CUR);

        bool header_state = true;

        while(!gzeof(gzDataPtr)){

            stringstream ss1;
            int docSize = 0;


            //New data
            //Read chunk size
            //Process header and content for that
            //Move to next header in the chunk
            //If no data left, read another block
            string data = readChunk(gzDataPtr, data_file, CHUNK_SIZE);



            pair<string, int> page = parse_header(data);
            string current_url = page.first;
            int content_length = page.second;

            if(content_length == -1){
                cout<<"Content problem";
                break;
            }

            map<string, posting> doc_postings;

            //New data end


            string htmlpage = readChunk(gzDataPtr, data_file, content_length);

            // Extract tokens and make postings
            ss1.clear();
            ss1.str(htmlpage);
            string term_detail;
            while(getline(ss1, term_detail, '\n')) {
                stringstream ss2(term_detail);
                string temp_term;
                while(ss2 >> temp_term) {

                    string term;

                    for(int i=0;i<temp_term.size();i++){
                        if(isalnum(temp_term[i]))
                            term.push_back(temp_term[i]);
                    }

                    if(term == "")
                        continue;

                    if(doc_postings.find(term) != doc_postings.end()) {
                        // term seen
                        doc_postings[term].frequency++;
                    }
                    else {
                        posting p;
                        p.docID = docID;
                        p.term = term;
                        p.frequency = 1;

                        doc_postings[term] = p;
                    }
                }

                docSize++;

            }

            string docstring = to_string(docID) + " " + current_url + " " + to_string(docSize) + "\n";
            doc_ptr << docstring;

            for(map<string, posting>::iterator it=doc_postings.begin();it != doc_postings.end(); it++) {
                postings.push_back(it->second);

                // Check for memory size
                if( postings.size()*sizeof(posting) > MAX_MEMORY) {

                    if(DEBUG)
                        cout<<"Memory full, writing to disk....\n";
                    // Sort the postings
                    stable_sort(postings.begin(), postings.end(), compare_postings);
                    // Write to disk
                    write_postings_to_disk(tempID++, postings);
                     
                    // Clear postings
                    postings.clear();
                }
            }

            doc_postings.clear();

            docID++;
        }

        zipped_file_index++;
    }

    //Write last list of postings to file
    if(DEBUG)
        cout<<"Writing last file for a zipped file\n";

    stable_sort(postings.begin(), postings.end(), compare_postings);
    write_postings_to_disk(tempID++, postings);

    doc_ptr.close();


    cout<<endl;
    return 0;
}
