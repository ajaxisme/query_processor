CC = g++
CFLAGS = -lz

all: indexer make_index query_processor

indexer: indexer.cpp indexer.o
	$(CC) indexer.cpp -o indexer $(CFLAGS)

make_index: make_index.cpp make_index.o
	$(CC) make_index.cpp -o make_index
	
query_processor: query_processor.cpp query_processor.o
	$(CC) query_processor.cpp -o query_processor

cleanup:
	rm -f *.o
	rm -f make_index
	rm -f indexer
	rm -f query_processor
