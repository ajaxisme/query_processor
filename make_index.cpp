#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <fstream>
#include <sstream>
#include <vector>

using namespace std;

#define DEBUG 1
#define BLOCK_SIZE 127

struct posting{
    long long docID;
    int frequency;
};

struct word_index{
    string term;
    long long start_pos;
    long long end_pos;
};

// Compress a single integer using varbyte compression
vector<unsigned char> compress_int(long long n){
    vector<unsigned char> compressed;
    while(n>0) {
        unsigned char a = n%128;
        n = n/128;
        if(n>0)
            a = a + 128;
        compressed.insert(compressed.begin(), a);
    }

    return compressed;
}

// Function to write an inverted list to disk
void write_to_index(ofstream& out, vector<posting> single_term){

    int terms = 0;
    string s;

    // Compress the first docID of the block as it is
    vector<unsigned char> compressed_docID = compress_int(single_term[0].docID);
    long long prev_docID = single_term[0].docID;

    vector<unsigned char> compressed_frequency = compress_int(single_term[0].frequency);
    terms++;

    for(long long i=1;i<single_term.size();i++){

        // Compress the docID as immediate difference from the previous element

        if(terms < BLOCK_SIZE){
            vector<unsigned char> compressed_doc = compress_int(single_term[i].docID - prev_docID);
            prev_docID = single_term[i].docID;
            compressed_docID.insert(compressed_docID.end(), compressed_doc.begin(), compressed_doc.end());

            vector<unsigned char> compressed_freq = compress_int(single_term[i].frequency);
            compressed_frequency.insert(compressed_frequency.end(), compressed_freq.begin(), compressed_freq.end());

            terms++;
        }
        else {

            // Write block to file
            
            s.assign(compressed_docID.begin(), compressed_docID.end());
            compressed_docID.clear();
            out<<s<<char(0);
            s.clear();

            //Write first docID of next block
            compressed_docID = compress_int(single_term[i].docID);
            prev_docID = single_term[i].docID;

            s.assign(compressed_frequency.begin(), compressed_frequency.end());
            compressed_frequency.clear();
            out<<s<<char(0);
            s.clear();

            //Write first frequency next block
            compressed_frequency = compress_int(single_term[i].frequency);

            terms=1;
        }
    }

    // Write last block to file
    for(int i=0;i<compressed_docID.size();i++){
       s.assign(compressed_docID.begin(), compressed_docID.end());
       compressed_docID.clear();
       out<<s<<char(0);
       s.clear();

       s.assign(compressed_frequency.begin(), compressed_frequency.end());
       compressed_frequency.clear();
       out<<s;
       s.clear();
    }
}

int main() {

    ifstream in ("temp_inverted_index", ios::in | ios::binary);

    string current_word;
    string line;
    vector<posting> single_term;
    vector<word_index> word_list;

    ofstream out ("compressed_inverted_index", ios::out | ios::binary);
    ofstream word_file ("word_index", ios::out | ios::binary);

    getline(in, line);
    stringstream ss;
    ss.str(line);
    long long docID;
    int frequency;
    string term;

    while(ss >> term) {
        ss >> docID;
        ss >>frequency;
    }

    current_word = term;
    posting p;
    p.docID = docID;
    p.frequency = frequency;

    single_term.push_back(p);

    while(getline(in, line)) {
        ss.clear();
        ss.str(line);
        while(ss >> term) {
            ss >> p.docID;
            ss >> p.frequency;
        }

        if(term == current_word) {
            single_term.push_back(p);
        }
        else {

            long long start_pos = out.tellp();
            start_pos = start_pos + 1;
            write_to_index(out, single_term);
            long long end_pos = out.tellp();
            out<<'\n';

            word_file << current_word << " " <<start_pos << " " << end_pos << " " << single_term.size() << "\n";

            single_term.clear();
            current_word = term;
            single_term.push_back(p);
        }
    }

    long long start_pos = out.tellp();
    start_pos = start_pos + 1;
    write_to_index(out, single_term);
    long long end_pos = out.tellp();
    out<<'\n';

    word_file << current_word << " " << start_pos << " " << end_pos << " " << single_term.size() << "\n";

    single_term.clear();

    in.close();
    out.close();
    word_file.close();

    return 0;
}
